function string1(arr) {
    if(!Array.isArray(arr) || arr == undefined){
        return [];
    }
    let resArr = [];
    for (let i in arr) {
        let word = arr[i].replace(/[$,]/g, '');
        if (isNaN(word)) {
            return 0;
        }
        resArr.push(Number(word));
    }
    return resArr;
}

module.exports = string1;
