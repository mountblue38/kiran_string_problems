function string5(arr) {
    if (!Array.isArray(arr) || arr == undefined) {
        return [];
    }
    let resStr = "";
    for (let i in arr) {
        resStr += arr[i] + " ";
    }
    return resStr;
}

module.exports = string5;
