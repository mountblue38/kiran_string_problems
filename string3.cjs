function string3(str) {
    if (typeof (str) != 'string' || str == undefined) {
        return [];
    }
    let resArr = str.split('/');
    if (resArr.length == 3) {
        for (i in resArr) {
            if (isNaN(resArr[i])) {
                return [];
            }
            resArr[i] = Number(resArr[i]);
        }
        if (resArr[0] <= 31 && resArr[0] > 0 && resArr[1] <= 12 && resArr[1] > 0 && resArr[2] > 0) {
            // if(0<resArr[0]>=31 && 0<resArr[1] >= 12 && resArr[2]>0){
            let months = ["January", "Febraury", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            return [months[resArr[1] - 1]];
        }
        return [];
    }
    else {
        return [];
    }
}

module.exports = string3;
