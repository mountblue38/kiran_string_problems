function string4(obj) {
    if (typeof (obj) != "object" || obj == undefined) {
        return [];
    }
    let resArr = "";
    for (let i in obj) {
        resArr += obj[i].charAt(0).toUpperCase() + obj[i].substr(1).toLowerCase() + " ";
    }
    return resArr;

}

module.exports = string4;
