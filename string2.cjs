function string2(str) {
    if(typeof(str) != 'string' || str == undefined){
        return [];
    }
    let resArr = str.split('.');
    for (i in resArr) {
        if ( resArr.length != 4 || isNaN(resArr[i])) {
            return [];
        }
        resArr[i] = Number(resArr[i]);
    }
    return resArr;
}

module.exports = string2;
